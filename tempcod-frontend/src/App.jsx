import { BrowserRouter, Route, Routes } from "react-router-dom"
import { Button, Container, Navbar } from 'react-bootstrap';

import Authorization from './pages/authorization/Authorization';
import NotFound from './pages/notFound/NotFound';
import Users  from './pages/users/Users';
import Card from "./pages/card/Card";

import './App.css';

import TempCodLogo from './assets/tempcod_logo.svg';
import exit_icon from './assets/exit_icon.svg';


function App() {
  return (

    <div className="App">
      <Navbar id="nav" expand="lg">
        <Container>
          <Navbar.Brand href="#">
            <img href="/users" className="tempcod_logo" src={TempCodLogo}
            alt="tempcod_logo"/>
          </Navbar.Brand>
          <Button className="languagePicker" variant="outline-primary">RU</Button>{' '}
          <Button variant="outline-dangerous">
            <img src={exit_icon} className="exit_icon" alt="exit_icon"/>
          </Button>{' '}
        </Container>
      </Navbar>

      <a id="auth" href="/authorization">Auth</a>
      <a id="users" href="/users">Users</a>
      <a id="card" href="/card">Card</a>
      <a className="notFoundLabel" href="/test1">Page not found</a>

      <header className="header">
        <BrowserRouter>
          <Routes>
            <Route path="/Authorization" element={<Authorization />} />
            <Route path="/Card" element={<Card />} />
            <Route path='/Users' element={<Users/>} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </BrowserRouter>
      </header>

    </div>
  );
}

export default App;
