import { Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

import Logo from '../../assets/logo.png';
import styles from "./Authorization.module.scss";


const Authorization = () => {

  const navigate = useNavigate();
  const onSubmit = (e) => {
    e.preventDefault()
    navigate.push('/users');
  }

  return (
    <Form className={styles.form}>

      <img className="logo" src={Logo} alt="Logo"/>

      <h6 align="center" className={styles.headline}>
        Авторизация
      </h6>

      <Form.Group className="mb-3" controlId="email">
        <Form.Label className="emailLabel">Имя пользователя</Form.Label>
        <Form.Control id="formBasicEmail" type="email"/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label className="passwordLabel">Пароль</Form.Label>
        <Form.Control id="formBasicPassword" type="password">
        </Form.Control>
      </Form.Group>

      <Form.Group className="mb-3" controlId="checkbox">
        {['checkbox'].map((type) => (
          <div key={`inline-${type}`} className="mb-3">
            <Form.Check
              className={styles.checkbox}
              inline
              name="group1"
              type={type}
              id="checkbox1"
            />
            <Form.Label className={styles.checkboxLabel}>Соглашаюсь с
              <a className={styles.terms} href="/test2">Условиями использования</a>
            </Form.Label>
          </div>
        ))}
      </Form.Group>

      <Button onSubmit={() => navigate("/users")} id="submit"
      className={styles.submit} variant="primary"
      type="submit">
        Войти
      </Button>

    </Form>
  );
};

export default Authorization;
