import { Container, Button, Form, Table, Col, Row, DropdownButton, Dropdown,
ProgressBar, Modal } from 'react-bootstrap';
import { jsPDF } from "jspdf";
import ReactDeleteRow from 'react-delete-row';

import logo_2 from '../../assets/logo_2.svg';
import battery_icon from '../../assets/battery_icon.svg';

import React, { useState } from 'react';
import styles from "./Users.module.scss";

const doc = new jsPDF();


const Users = () => {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const data = [
    { id: '1', name: 'Александров Александр', temp: '35.76 °C (27 июля 2021 г., 10:57)',
    uid: '29', battery: <ProgressBar id="battery_status_bar" now={60} />,
    checkbox: <Form.Check aria-label="option 1" /> },
    { id: '2', name: 'Борисов Владимир', temp: '35.76 °C (27 июля 2021 г., 10:57)',
    uid: '12', battery: <ProgressBar id="battery_status_bar" now={25} />,
    checkbox: <Form.Check aria-label="option 1" /> },
    { id: '3', name: 'Гаврилова Елизавета', temp: '38.26 °C (27 июля 2021 г., 09:12)',
    uid: '32', battery: <ProgressBar id="battery_status_bar" now={90} />,
    checkbox: <Form.Check aria-label="option 1" /> },
  ];

  return (

    <Container className={styles.users_table_container} id="users_table_container">

      <Row>
        <Col sm={5}>
          <img id="logo_2" align="left" src={logo_2} className="logo_2"
          alt="logo_2" />
        </Col>

        <Col sm={4}>
          <Form.Control className={styles.search_input} id="search_input"
           type="search" placeholder="Search"
           aria-label="Search">
          </Form.Control>
        </Col>

        <Col sm={2}>
          <Row>
            <DropdownButton className={styles.download_dropdown_button}
            id="download_dropdown_button" title="Скачать">
              <Dropdown.Item href="#/action-1">CSV</Dropdown.Item>
              <Dropdown.Item href="#/action-2">
                PDF
              </Dropdown.Item>
              <Dropdown.Item href="#/action-3">XLSX</Dropdown.Item>
            </DropdownButton>
          </Row>
        </Col>

        <Col sm={1}>
          <Button onClick={handleShow} className={styles.delete_user_button}
          id="delete_user_button"variant="primary">
          </Button>{' '}
        </Col>

      </Row>

      <Table className={styles.table_users} id="table_users" bordered hover>

        <thead>
          <tr>
            <th align="center">#</th>
            <th>ФИО или табельный номер</th>
            <th>Температура</th>
            <th>UID устр-ва</th>
            <th>Заряд</th>
            <th align="center">
              <Form.Check  aria-label="option 1" />
            </th>
            <th>Удалить</th>
          </tr>
        </thead>

        <tbody>

          <tr>
            <td>1</td>
            <td>Александров Александр</td>
            <td>
              <text id="example1_temp_blue">35.76 °C (27 июля 2021 г., 10:57)
              </text>
            </td>
            <td>29</td>
            <td>
              <img id="battery_icon" src={battery_icon}
              className="battery_icon" alt="battery_icon" /> 35%
            </td>
            <td>
              <Form.Check aria-label="option 1" />
            </td>
          </tr>

          <tr>
            <td>2</td>
            <td>Борисов Владимир</td>
            <td>36.67 °C (27 июля 2021 г., 11:23)</td>
            <td>12</td>
            <td>
              <img id="battery_icon" src={battery_icon}
              className="battery_icon" alt="battery_icon" /> 15%
            </td>
            <td>
              <Form.Check aria-label="option 1" />
            </td>
          </tr>

          <tr>
            <td>3</td>
            <td>Гаврилова Елизавета</td>
            <td>
              <text id="example1_temp_red">38.26 °C (27 июля 2021 г., 09:12)
              </text>
            </td>
            <td>32</td>
            <td>
              <img id="battery_icon" src={battery_icon}
              className="battery_icon" alt="battery_icon" /> 42%
            </td>
            <td>
              <Form.Check aria-label="option 1" />
            </td>
          </tr>

        </tbody>

      </Table>

      <Table className={styles.table_users} id="table_users" bordered hover>

        <thead>
          <tr>
            <th align="center">#</th>
            <th>ФИО или табельный номер</th>
            <th>Температура</th>
            <th>UID устр-ва</th>
            <th>Заряд</th>
            <th>
              <Form.Check aria-label="option 1" />
            </th>
            <th>Удалить</th>
          </tr>
        </thead>

        <tbody>

          { data.map((item, i) => { return (
          <ReactDeleteRow key={i} data={item} onDelete={ item => { return window.confirm(`Удаление пользователя?`) }}>
              <td onClick={() => window.open("/card")}>{item.id}</td>
              <td onClick={() => window.open("/card")}>{item.name}</td>
              <td onClick={() => window.open("/card")}>{item.temp}</td>
              <td onClick={() => window.open("/card")}>{item.uid}</td>
              <td onClick={() => window.open("/card")}>{item.battery}</td>
              <td style={{ align: "center" }}>{item.checkbox}</td>
          </ReactDeleteRow>
          )}) }

        </tbody>

      </Table>

      <Modal className={styles.modal_del} id="modal_del"
      animation={false} show={show} onHide={handleClose}>

       <Modal.Header closeButton>
         <Modal.Title>Удаление пользователя</Modal.Title>
       </Modal.Header>

       <Modal.Body>
         <p>
           Вы уверены, что хотите удалить пользователя <b>Александров Александр?</b>
           <br />
           Действите нельзя будет отменить
         </p>
       </Modal.Body>

       <Modal.Footer>
         <Button id="cancel_button" variant="secondary" onClick={handleClose}>
           Отменить
         </Button>
         <Button id="confirm_button" variant="primary" onClick={handleClose}>
           Подтвердить
         </Button>
       </Modal.Footer>

     </Modal>

    </Container>

  );
};

export default Users;
