import {Button} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

import ErrorIcon from '../../assets/error_icon.svg';
import styles from "./NotFound.module.scss";


const NotFound = () => {

  const navigate = useNavigate();

  return (
    <header className="header">
      <img src={ErrorIcon} className="logo" alt="logo" />
      <h4 className={styles.notFoundHeadline}>
        PAGE NOT FOUND
      </h4>
      <p className={styles.error}>
        The content you're looking for doesn't exist.<br />
        Either it was removed, or you mistyped the link.
      </p>
      <div className={styles.back}>
        <Button onClick={() => navigate(-1)} variant="primary">
          <span>Go Back</span>
        </Button>{' '}
      </div>
    </header>
  );
};

export default NotFound;
