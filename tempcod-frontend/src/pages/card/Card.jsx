import { Line } from 'react-chartjs-2';
import styles from "./Card.module.scss";
import Moment from "react-moment";
import { EditText, EditTextarea } from 'react-edit-text';
import EdiText from 'react-editext'

import 'react-edit-text/dist/index.css';

import tools_icon from '../../assets/tools_icon.svg';
import status_user_active_icon from '../../assets/status_user_active_icon.svg';

import { Container, Table, Col, Row, DropdownButton, Dropdown }
from 'react-bootstrap';

import {getBackgroundGradient, getBorderGradient, getRandomPoints, getTimeLabels}
from "../../utils/charts";

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Filler
} from 'chart.js';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Tooltip, Filler);


const Card = () => {

  const data = [
    { id: '27 июля 2021 г., 10:57', name: '35.6 °C', temp: 'UID: 29'},
    { id: '27 июля 2021 г., 10:57', name: '36.9 °C', temp: 'UID: 29'},
    { id: '27 июля 2021 г., 10:57', name: '38.6 °C', temp: 'UID: 29'},
  ];

  return (

    <Container id="user_info" className={styles.user_info}>

      <Row id="row_1">

        <Col>
          <a href="/users" id="back_to_users_buttons"
          lassName={styles.back_to_users_buttons}>
            к списку пользователей
          </a>{' '}
        </Col>

        <Col md="auto">
        </Col>

        <Col xs lg="3">
          <p id="update_text">Обновлено &nbsp;
            <div className="App">
              <Moment format="DD июля YYYY г., HH:mm:ss" interval={0} />
            </div>
            <br />
          </p>
        </Col>

     </Row>

     <Row id="row_2">

       <Col xs={6} md={3}>
         <img id="status_user_active_icon" align="left" src={status_user_active_icon}
          className="status_user_active_icon" disabled alt="status_user_active_icon" />
          <h3 id="name_of_user">
            Екатерина
          </h3>
       </Col>

       <Col xs={6} md={5}>
       </Col>

       <Col xs={6} md={4}>
         <img id="tools_icon" align="left" src={tools_icon}
         className="tools_icon" disabled alt="tools_icon" />
         <h3 id="temp_of_user">
           35.9°C
         </h3>
       </Col>

     </Row>

     <Row>

        <div className={styles.container}>
          <Line
            className={styles.graph}
            options={{
              responsive: true,
              maintainAspectRatio: false
            }}
            data={{
              labels: getTimeLabels(),
              datasets: [{
                label: "",
                data: getRandomPoints(),
                tension: 0.5,
                fill: true,
                borderColor: function(context) {
                  const chart = context.chart;
                  const {ctx, chartArea} = chart;
                  if (!chartArea) return null;
                  return getBorderGradient(ctx);
                },
                backgroundColor: function(context) {
                  const chart = context.chart;
                  const {ctx, chartArea} = chart;
                  if (!chartArea) return null;
                  return getBackgroundGradient(ctx);
                }
              }]
            }}
          />
       </div>

      </Row>

      <Container className={styles.dowbload_and_calendar_container}
      id="dowbload_and_calendar_container">

        <Row>
          <Col sm={4}>

          </Col>

          <Col sm={4}>

          </Col>

          <Col sm={4}>
            <Row>
              <DropdownButton className={styles.download_dropdown_button}
              id="download_dropdown_button" title="Скачать">
                <Dropdown.Item href="#/action-1">CSV</Dropdown.Item>
                <Dropdown.Item href="#/action-2">PDF</Dropdown.Item>
                <Dropdown.Item href="#/action-3">XLSX</Dropdown.Item>
              </DropdownButton>
            </Row>
          </Col>

        </Row>

      </Container>

      <Table className={styles.table_users} id="table_users" bordered hover>

        <thead>
          <tr>
            <th>Дата</th>
            <th>Измерение</th>
            <th>Устройство</th>
          </tr>
        </thead>

        <tbody>

          <tr>
            <td>27 июля 2021 г., 10:57</td>
            <td>
              <text>35.6 °C
              </text>
            </td>
            <td>UID: 29</td>
          </tr>

          <tr>
            <td>27 июля 2021 г., 10:57</td>
            <td>36.9 °C</td>
            <td>UID: 29</td>
          </tr>

          <tr>
            <td>27 июля 2021 г., 10:57</td>
            <td>
              <text id="example1_temp_red">38.6 °C
              </text>
            </td>
            <td>UID: 29</td>
          </tr>

        </tbody>

      </Table>

    </Container>
  )
}

export default Card;
