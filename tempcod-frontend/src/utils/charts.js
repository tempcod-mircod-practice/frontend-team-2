export const getTimeLabels = ({
  startingFrom = new Date(),
  count = 19,
  minutesRange = 30
} = {}) => {
  const date = new Date(startingFrom);
  const array = [];
  for (let i = 0; i < count; i++) {
    date.setMinutes(date.getMinutes() - minutesRange);
    array.push(date.toTimeString().split(" ")[0])
  }
  return array
}

const randomIntFromInterval = (min, max) =>
  Math.floor((Math.random() * (max - min + 1) + min) * 10) / 10;

export const getRandomPoints = ({
  min = 35,
  max = 38,
  count = 19
} = {}) => {
  const array = [];
  for (let i = 0; i < count; i++) {
    array.push(randomIntFromInterval(min, max))
  }
  return array
}

export const getBorderGradient = (ctx) => {
  const gradient = ctx.createLinearGradient(0, 0, 0, 200);
  gradient.addColorStop(0, "#F08C83");
  gradient.addColorStop(0.5, "#8585BA");
  gradient.addColorStop(1, "#10A7C8");
  return gradient;
}

export const getBackgroundGradient = (ctx) => {
  const gradient = ctx.createLinearGradient(0, 0, 0, 200);
  gradient.addColorStop(0, "#F3F0FF");
  gradient.addColorStop(1, "#F8DED4");
  return gradient;
}
